/*
 * MIT License
 *
 * Copyright (c) 2011-2018 Pedro Henrique Penna <pedrohenriquepenna@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.  THE SOFTWARE IS PROVIDED
 * "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include <mppaipc.h>

#include <nanvix/syscalls.h>
#include <nanvix/limits.h>
#include <nanvix/pm.h>

/**
 * @brief Number of processes.
 */
static int nclusters = 0;

/**
 * @brief Node number.
 */
static int nodenum = -1;

/*============================================================================*
 * Utilities                                                                  *
 *============================================================================*/

/**
 * @brief ID of slave processes.
 */
static int pids[NANVIX_PROC_MAX];

/**
 * @brief Barrier for global synchronization.
 */
static int barrier;

/**
 * @brief Spawns remote processes.
 */
static void spawn_remotes(void)
{
	char master_node[4];
	char nclusters_str[4];
	int nodes[nclusters + 1];
	const char *argv[] = {
		"/slave",
		master_node,
		nclusters_str,
		NULL
	};

	/* Build nodes list. */
	nodes[0] = nodenum;
	for (int i = 0; i < nclusters; i++)
		nodes[i + 1] = i;

	/* Create global barrier. */
	assert((barrier = barrier_create(nodes, nclusters + 1)) >= 0);

	/* Spawn remotes. */
	sprintf(master_node, "%d", nodenum);
	sprintf(nclusters_str, "%d", nclusters);
	for (int i = 0; i < nclusters; i++)
		assert((pids[i] = mppa_spawn(i, NULL, argv[0], argv, NULL)) != -1);
}

/**
 * @brief Wait for remote processes.
 */
static void join_remotes(void)
{
	/* Sync. */
	assert(barrier_wait(barrier) == 0);

	for (int i = 0; i < nclusters; i++)
		assert(mppa_waitpid(pids[i], NULL, 0) != -1);

	/* House keeping. */
	assert(barrier_unlink(barrier) == 0);
}

/*============================================================================*/

/**
 * @brief HAL RMem Microbenchmark Driver
 */
int main2(int argc, const char **argv)
{
	assert(argc == 2);

	/* Retrieve kernel parameters. */
	nclusters = atoi(argv[1]);

	nodenum = sys_get_node_num();

	printf("hello from node %d\n", nodenum);

	spawn_remotes();
	join_remotes();

	return (EXIT_SUCCESS);
}
