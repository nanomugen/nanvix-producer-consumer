#include "index.h"
#include <pthread.h>


void QueuePush(struct Queue** queue,struct Node* node){
  if(queue == NULL||*queue == NULL) return;
  struct Atom *atom = malloc(sizeof(struct Atom));
  atom->node = node;
  atom->next=NULL;
  atom->queue = *queue;
  pthread_mutex_lock(&queue_mutex);
  if((*queue)->next==NULL){
    (*queue)->next=atom;
  }
  else{
    (*queue)->last->next = atom;
  }
  (*queue)->last = atom;
  pthread_cond_signal(&pop_cond);
  //(*queue)->size++;
  //(*queue)->maxSize = ((*queue)->size > (*queue)->maxSize) ? (*queue)->size : (*queue)->maxSize;
  pthread_mutex_unlock(&queue_mutex);

}

void QueuePop(struct Queue** queue, struct Node** ret){
  if(queue == NULL || *queue == NULL) return;

  pthread_mutex_lock(&queue_mutex);
  while((*queue)->next==NULL){
    if(--((*queue)->active)<=0){
      pthread_mutex_unlock(&queue_mutex);
      ret=NULL;
      return;
    }
    ((*queue)->inactive)++;
    pthread_cond_wait(&pop_cond,&queue_mutex);
    ((*queue)->active)++;
  }
  ((*queue)->inactive)--;
  struct Node* node = (*queue)->next->node;
  struct Atom* atom = (*queue)->next->next;
  //da pra tirar o free do unlock, não sei se compensa
  free((*queue)->next);
  (*queue)->next=atom;
  //(*queue)->size--;
  pthread_mutex_unlock(&queue_mutex);
  *ret = node;
}

//pop usado no RTREESEARCH5

struct Node* QueuePop2(struct Queue** queue){
  if(queue == NULL || *queue == NULL) return;
  pthread_mutex_lock(&queue_mutex);
  while((*queue)->next==NULL){
    if(--((*queue)->active)<=0){
       pthread_mutex_unlock(&queue_mutex);
       pthread_cond_broadcast(&pop_cond);
       return kill;
    }
    ((*queue)->inactive)++;
    pthread_cond_wait(&pop_cond,&queue_mutex);
    ((*queue)->active)++;
  }
  ((*queue)->inactive)--;
  struct Node* node = ((*queue)->next->node);
  struct Atom* atom = (*queue)->next->next;
  free((*queue)->next);
  (*queue)->next=atom;
  pthread_mutex_unlock(&queue_mutex);

  return node;
}



void QueueInit(struct Queue** queue) {
    *queue = calloc(1, sizeof(struct Queue));
    pthread_mutex_init(&queue_mutex,NULL);
    pthread_cond_init(&pop_cond,NULL);
}


void AtomKill(struct Atom** atom){
  struct Atom* aux;
  int test=0;
  if((*atom) != NULL){
    test=1;
    aux = (*atom)->next;
  }
  free (*atom);
  if(test==1)
    AtomKill(&aux);
}

void QueueKill(struct Queue** queue){
  AtomKill(&((*queue)->next));
  free(*queue);
  *queue = NULL;
  pthread_mutex_destroy(&queue_mutex);
  pthread_cond_destroy(&pop_cond);
}
